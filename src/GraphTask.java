
import java.util.*;

/**Koostada meetod etteantud sidusa lihtgraafi G tsentri leidmiseks.
 *
 * Kasutatud materjalid:
 * 1. https://et.wikipedia.org/wiki/Sidus_graaf
 * 2. http://algs4.cs.princeton.edu/41graph/BreadthFirstPaths.java.html
 * 3. http://mathworld.wolfram.com/GraphCenter.html
 * 4. Buldas, Laud, Villemson. Graafid (käsikiri, 2008, vt. Moodle)
 * 5. https://github.com/CarterZhou/algorithms_practice/blob/master/algorithms4th/graph/undirected/GraphProperties.java
 *
 *
 * @author TaaviTilk
 * @author Jaanus Pöial
 *
 */

public class GraphTask {
    static String centerID = null;
    Graph g = new Graph ("G");
    public static void main (String[] args) {

        GraphTask a = new GraphTask();

        a.run();
        System.out.println("Tsentri asukohaks on: "+centerID);
        //throw new RuntimeException ("Nothing implemented yet!"); // delete this
    }

    /**
     *
     */
    public void run() {

//      /*Random graph */
//      g.createRandomSimpleGraph (6, 9);


//        test1();
//        test2();
//        test3();
        test4();
//        test5();



        center();

        System.out.println (g);

        // TODO!!! Your experiments here
    }

    private void test1(){
                /*Test 1*/

        Vertex a = new Vertex("A");
        g.first = a;
        a.first = null;


    }
    private void test2(){
                /*Test 2*/
        Vertex a = new Vertex("A"); // nimetame nurga
        Vertex b = new Vertex("B");

        g.first = a; // anname algus nurga
        a.next = b; // jargmised nurgad

        Arc ab = new Arc("AB");
        Arc ba = new Arc("BA");

        a.first = ab;
        ab.target = b;

        b.first = ba;
        ba.target = a;

    }

    private void test3(){
                /*Test 3*/
        Vertex a = new Vertex("A");
        Vertex b = new Vertex("B");
        Vertex c = new Vertex("C");
        Vertex d = new Vertex("D");

        g.first = a;
        a.next = b;
        b.next = c;
        c.next = d;



        Arc ab = new Arc("AB");
        Arc ba = new Arc("BA");

        Arc ac = new Arc("AC");
        Arc ca = new Arc("CA");

        Arc ad = new Arc("AD");
        Arc da = new Arc("DA");

        a.first = ab;
        ab.target = b;
        ab.next = ac;
        ac.target =c;
        ac.next = ad;
        ad.target =c;

        b.first = ba;
        ba.target = a;

        c.first = ca;
        ca.target = a;

        d.first = da;
        da.target = a;

    }
    private void test4(){
                /*Test 4*/

       Vertex a = new Vertex("A"); // nimetame nurga
       Vertex b = new Vertex("B");
       Vertex c = new Vertex("C");
       Vertex d = new Vertex("D");
       Vertex e = new Vertex("E");

       g.first = a; // anname algus nurga
       a.next = b; // jargmised nurgad
       b.next = c;
       c.next = d;
       d.next = e;

       Arc ab = new Arc("AB");
       Arc ba = new Arc("BA");

       Arc ac = new Arc("AC");
       Arc ca = new Arc("CA");

       Arc ad = new Arc("AD");
       Arc da = new Arc("DA");

       Arc bc = new Arc("BC");
       Arc cb = new Arc("CB");

       Arc bd = new Arc("BD");
       Arc db = new Arc("DB");

       Arc be = new Arc("BE");
       Arc eb = new Arc("EB");

       Arc cd = new Arc("CD");
       Arc dc = new Arc("DC");


       a.first = ab;
       ab.target = b;
       ab.next = ac;
       ac.target = c;
       ac.next = ad;
       ad.target = d;

       b.first = ba;
       ba.target = a;
       ba.next = bc;
       bc.target = c;
       bc.next = bd;
       bd.target = d;
       bd.next = be;
       be.target = e;

       c.first = ca;
       ca.target = a;
       ca.next = cb;
       cb.target = b;
       cb.next = cd;
       cd.target = d;

       d.first = da;
       da.target = a;
       da.next = db;
       db.target = b;
       db.next = dc;
       dc.target = c;

       e.first = eb;
       eb.target = b;

    }

    private void test5 (){
               /*Test 5*/

        Vertex a = new Vertex("A"); // nimetame nurga
        Vertex b = new Vertex("B");
        Vertex c = new Vertex("C");
        Vertex d = new Vertex("D");
        Vertex e = new Vertex("E");

        g.first = a; // anname algus nurga
        a.next = b; // jargmised nurgad
        b.next = c;
        c.next = d;
        d.next = e;

        Arc ab = new Arc("AB");
        Arc ba = new Arc("BA");

        Arc ac = new Arc("AC");
        Arc ca = new Arc("CA");

        Arc ae = new Arc("AE");
        Arc ea = new Arc("EA");

        Arc bd = new Arc("BD");
        Arc db = new Arc("DB");

        Arc be = new Arc("BE");
        Arc eb = new Arc("EB");

        Arc de = new Arc("DE");
        Arc ed = new Arc("ED");

        Arc cd = new Arc("CD");
        Arc dc = new Arc("DC");

        Arc ce = new Arc("CE");
        Arc ec = new Arc("EC");

        a.first = ab;
        ab.target = b;
        ab.next = ac;
        ac.target = c;
        ac.next = ae;
        ae.target = d;

        b.first = ba;
        ba.target = a;
        ba.next = be;
        be.target = e;
        be.next = bd;
        bd.target = d;


        c.first = ca;
        ca.target = a;
        ca.next = ce;
        ce.target = e;
        ce.next = cd;
        cd.target = d;

        d.first = dc;
        dc.target = c;
        dc.next = db;
        db.target = b;
        db.next = de;
        de.target = e;

        e.first = ea;
        ea.target = a;
        ea.next = eb;
        eb.target = b;
        eb.next = ec;
        ec.target = c;
        ec.next = ed;
        ed.target = d;

    }


    /**
     * tsentri arvutamine
     *
     */
    private void center(){
        ArrayList<Vertex> nurgad = new ArrayList();
        nurgad = g.getVertexList();
        int center = Integer.MAX_VALUE;
        centerID = "";
        for (Iterator<Vertex> iterator = nurgad.iterator(); iterator.hasNext();){

            Vertex nurk = iterator.next();

            int temp = g.breadthFirst(nurk);
            if (temp < center){
                center = temp;
                centerID = nurk.id;
            }
        }
    }


    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        private int kaugus;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        /**
         * määra Vertexi värv
         * 1 - hall
         * 2 - must
         * @param i
         */
        public void vertexiVarv(int i) {
            this.info = i;// TODO Auto-generated method stub

        }

        /**
         * Vertexi kauguse omistamine
         * @param i
         */
        public void setVdist(int i) {
            this.kaugus = i;// TODO Auto-generated method stub

        }

        /**
         * Pöördub järgmise tipu poole
         * @return
         */
        public Vertex getNext() {
            return next;
        }

        /**
         * väljuvate kaarte arv
         * @return kaared
         */
        public ArrayList<Arc> valjuvKaar() {
            ArrayList<Arc> kaared = new ArrayList();
            Arc kaar = first;
            if(kaar == null){
                return null;
            }
            try {
                kaared.add(kaar);
                while (kaar.next != null) {
                    kaar = kaar.next;
                    kaared.add(kaar);
                }
            }catch (NullPointerException e) {
                System.out.println("Kaari rohkem ei olnud");
            }


            return kaared;
        }

        /**
         *
         * @return Vertexivarv 1 2
         */
        public int kysiVertexiVarv() {
            // TODO Auto-generated method stub
            return info;
        }

        // TODO!!! Your Vertex methods here!
    }

    /**
     * Kaarte klass
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info;

        /**
         * sisenditele muutujate omaistamine
         * @param s id
         * @param v suubub
         * @param a jargime
         */
        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        /**
         * Kaar siendiga s,
         * @param s
         */
        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        /**
         * Nurgast kuhu suubdub
         * @return nurga Vertex
         */
        public Vertex kuhuSuundub() {
            // TODO Auto-generated method stub
            return target;
        }

        // TODO!!! Your Arc methods here!
    }

    /**
     * Klass Graafid
     */
    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        /**
         * Kasuta juhul, kui soov anda nurk ette.
         * @return
         */
//        public Vertex getFirst() {
//            return first;
//        }

        /**
         * graafi loetavale kujule viimine pritntimisel
         * @return
         */
        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        /**
         * Vertex nurga loomine
         * @param vid
         * @return
         */
        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        /**
         * Kaare loomine
         * @param aid
         * @param from
         * @param to
         * @return
         */
        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         *
         * Create a connected undirected random tree with n vertices. Each new
         * vertex is connected to some random existing vertex.
         *
         * @param n
         *            number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_" + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_" + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph. Side effect: corrupts info
         * fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple arcs)
         * random graph with n vertices and m edges.
         *
         * @param n
         *            number of vertices
         * @param m
         *            number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);
                int j = (int) (Math.random() * n);
                if (i == j)
                    continue;
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;
            }
        }


        // TODO!!! Your Graph methods here!

        /**
         * Traverse the graph breadth-first. Execute vertWork (v) for each
         * vertex v.
         *  Kasutatud algorithm
         *  Kasutatud näited
         * https://github.com/CarterZhou/algorithms_practice/blob/master/algorithms4th/graph/undirected/GraphProperties.java
         * http://algs4.cs.princeton.edu/41graph/BreadthFirstPaths.java.html
         * @param algus punkt verteksil
         * @return ektsentriku, et arvutada tsenter
         *
         */

        public int breadthFirst(Vertex algus) {

            if (!getVertexList().contains(algus))
                throw new RuntimeException("See on teise graafi tipp");
            if (getVertexList().isEmpty())
                throw new RuntimeException("Graafi pole sisestatud");
            if (getVertexList().size() == 1)
                return 0;
            if (algus.valjuvKaar().size() < 1)
                return 0;

            int ektsentrik = 0;
            this.initgraph();
            Queue<Vertex> qlist = new LinkedList(); //siia lahevad nurgad sisse
            qlist.add(algus);
            algus.vertexiVarv(1); // maarame jouga alguse halliks "1"

            while (qlist.size() > 0) {
                Vertex kaaretipp = qlist.poll(); //  valja FIFO
                ArrayList<Arc> valjuvadkaared = kaaretipp.valjuvKaar();
                if(valjuvadkaared == null){
                    break;
                }
                // palju tipul kaari on
                for (Iterator iterator = valjuvadkaared.iterator(); iterator.hasNext();) {
                    Arc jargmineKaar = (Arc) iterator.next(); //jargmine kaar, mis valjuvKaar() listis on
                    Vertex suubub = jargmineKaar.kuhuSuundub();
                    if (suubub.kysiVertexiVarv() == 0) {  // kui vetexit ei ole kylastatud
                        suubub.kaugus = kaaretipp.kaugus + 1;
                        if (ektsentrik < suubub.kaugus){
                            ektsentrik = suubub.kaugus;
                        }
                        qlist.add(suubub);
                        suubub.vertexiVarv(1);
                    }
                    kaaretipp.vertexiVarv(2); //maarame jouga alguse mustaks "2"
                }
            }
            return ektsentrik;

        }

        /**
         * Graafide nurkade list
         * @return
         */
        private ArrayList<Vertex> getVertexList() {
            ArrayList<Vertex> list = new ArrayList<Vertex>();
            Vertex v = this.first;
            list.add(v);
            while (v.next != null) {
                list.add(v.getNext());
                v = v.getNext();
            }
            return list;
        }

        /**
         * Graafi nurkade initsialeerimin ehk varvimine, kui juba kylastatud.
         */
        public void initgraph() {
            ArrayList<Vertex> vlist = getVertexList();
            for (int i = 0; i < vlist.size(); i++) {
                vlist.get(i).vertexiVarv(0);// "valge"
                vlist.get(i).setVdist(0);
            }
        }

    }




} 
